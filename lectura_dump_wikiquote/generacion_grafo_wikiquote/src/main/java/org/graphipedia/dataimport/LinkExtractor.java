//
// Copyright (c) 2012 Mirko Nasato
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.
//
package org.graphipedia.dataimport;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import constantes.ConstantesGeneracionDeGrafoGiraph;

public class LinkExtractor extends SimpleStaxParser {

	private static final Pattern LINK_PATTERN = ConstantesGeneracionDeGrafoGiraph.LINK_PATTERN;

	private static final Pattern CATEGORY_PATTERN = ConstantesGeneracionDeGrafoGiraph.CATEGORY_PATTERN;

	private final XMLStreamWriter writer;
	private final ProgressCounter pageCounter = new ProgressCounter();

	private String title;
	private String text;

	public LinkExtractor(XMLStreamWriter writer) {
		super(Arrays.asList("page", "title", "text"));
		this.writer = writer;
	}

	public int getPageCount() {
		return pageCounter.getCount();
	}

	@Override
	protected void handleElement(String element, String value) {
		if ("page".equals(element)) {
			// Si es un titulo que no contiene un ":" o si es un titulo que se
			// corresponde con una categoria, lo listaremos como vertice
			if (!title.contains(":") || title.startsWith("Categoría")) {
				try {
					writePage(title, text);
				} catch (XMLStreamException streamException) {
					throw new RuntimeException(streamException);
				}
			}
			title = null;
			text = null;
		} else if ("title".equals(element)) {
			title = value;
		} else if ("text".equals(element)) {
			text = value;
		}
	}

	private void writePage(String title, String text) throws XMLStreamException {
		imprimirConstante("\t");
		writer.writeStartElement("p");
		System.out.println("Articulo: " + title);
		imprimirConstante("\n");
		imprimirConstante("\t");
		imprimirConstante("\t");
		writer.writeStartElement("t");
		writer.writeCharacters(title);
		writer.writeEndElement();
		imprimirConstante("\n");

		// write links
		Set<String> links = parseLinks(text);
		links.remove(title);
		for (String link : links) {
			imprimirConstante("\t");
			imprimirConstante("\t");
			writer.writeStartElement("l");
			writer.writeCharacters(link);
			writer.writeEndElement();
			imprimirConstante("\n");
		}

		// write categories
		Set<String> categorys = parseCategories(text);
		for (String category : categorys) {
			imprimirConstante("\t");
			imprimirConstante("\t");
			writer.writeStartElement("c");
			writer.writeCharacters(category);
			writer.writeEndElement();
			imprimirConstante("\n");
		}

		imprimirConstante("\t");
		writer.writeEndElement();
		imprimirConstante("\n");

		pageCounter.increment();
	}

	private Set<String> parseLinks(String text) {
		Set<String> links = new HashSet<String>();
		if (text != null) {
			Matcher matcher = LINK_PATTERN.matcher(text);
			while (matcher.find()) {
				String link = matcher.group(1);
				if (!link.contains(":")) {
					if (link.contains("|")) {
						link = link.substring(0, link.lastIndexOf('|'));
					}
					links.add(link);
					System.out.println("Enlace encontrado: " + link);
				}
			}
		}
		return links;
	}

	private Set<String> parseCategories(String text) {
		Set<String> categories = new HashSet<String>();
		if (text != null) {
			Matcher matcher = CATEGORY_PATTERN.matcher(text);
			while (matcher.find()) {
				String match = matcher.group();
				System.out.println("Categoría encontrada: "
						+ match.substring(12, match.length() - 2));
				categories.add(match.substring(2, match.length() - 2));
			}
		}
		return categories;
	}

	private void imprimirConstante(String constante) throws XMLStreamException {
		writer.writeCharacters(constante);
	}
}
