package tesina;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;

import org.graphipedia.dataimport.ProgressCounter;
import org.graphipedia.dataimport.SimpleStaxParser;

import constantes.ConstantesGeneracionDeGrafoGiraph;

/**
 * (Esta clase se genera en este proyecto y no en graphipedia, por que eclipse
 * no me permitía generar una clase en el proyecto graphiphedia.)
 * 
 * @author jlarroque
 *
 */
public class GraphExtractor extends SimpleStaxParser {

	private static final String DELIMITADOR_LINEA = "\n";

	private static final String DELIMITADOR_DENTRO_DE_LINEA = "\t";

	private static final Pattern LINK_PATTERN = ConstantesGeneracionDeGrafoGiraph.LINK_PATTERN;

	private static final Pattern CATEGORY_PATTERN = ConstantesGeneracionDeGrafoGiraph.CATEGORY_PATTERN;

	private final FileWriter writerVerticesEnlacesCategorias;

	// Solo escribe los vertices en un archivo de texto y nada mas
	private final FileWriter writerVertices;

	private List<String> expresionesRegulares;

	private final ProgressCounter pageCounter = new ProgressCounter();

	private String title;
	private String text;

	public GraphExtractor(FileWriter writerVerticesEnlacesCategorias,
			FileWriter writerVertices, List<String> expRegulares) {
		super(Arrays.asList("page", "title", "text"));
		this.writerVerticesEnlacesCategorias = writerVerticesEnlacesCategorias;
		this.writerVertices = writerVertices;
		this.expresionesRegulares = expRegulares;
	}

	public int getPageCount() {
		return pageCounter.getCount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.graphipedia.dataimport.SimpleStaxParser#handleElement(java.lang.String
	 * , java.lang.String)
	 */
	@Override
	protected void handleElement(String element, String value) {
		if ("page".equals(element)) {
			// Si es un titulo que no contiene un ":" o si es un titulo que se
			// corresponde con una categoria, lo listaremos como vertice
			if (!title.contains(":") || title.startsWith("Categoría")) {
				try {
					writePage(title, text);
				} catch (XMLStreamException streamException) {
					throw new RuntimeException(streamException);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
			title = null;
			text = null;
		} else if ("title".equals(element)) {
			title = value;
		} else if ("text".equals(element)) {
			text = value;
		}
	}

	private void writePage(String title, String text)
			throws XMLStreamException, IOException {

		// Inicio Vertice
		writerVertices.write(title + DELIMITADOR_LINEA);

		// El id es el titulo de la cita
		writerVerticesEnlacesCategorias.write(title
				+ DELIMITADOR_DENTRO_DE_LINEA);

		// El valor es 0, al menos por ahora
		writerVerticesEnlacesCategorias
				.write(0.0 + DELIMITADOR_DENTRO_DE_LINEA);

		Set<String> links = parseLinks(text);
		Set<String> categorys = parseCategories(text);

		// Inicio enlaces y categorias

		// Inicio enlaces
		links.remove(title);
		for (String link : links) {
			// Los enlaces en wikipedia pueden contener blancos antes o despues,
			// los cuales entorpecen el formato saliente del archivo de Giraph
			// (por eso se hace trim())
			writerVerticesEnlacesCategorias.write(link.trim()
					+ DELIMITADOR_DENTRO_DE_LINEA + "1.0"
					+ DELIMITADOR_DENTRO_DE_LINEA);
		}

		// Inicio categorias
		for (String category : categorys) {
			writerVerticesEnlacesCategorias.write(category
					+ DELIMITADOR_DENTRO_DE_LINEA + "2.0"
					+ DELIMITADOR_DENTRO_DE_LINEA);

		}

		// Fin enlaces y categorias

		// Fin vertice

		writerVerticesEnlacesCategorias.write(DELIMITADOR_LINEA);

		pageCounter.increment();
	}

	private Set<String> parseLinks(String text) {
		Set<String> links = new HashSet<String>();
		if (text != null) {
			Matcher matcher = LINK_PATTERN.matcher(text);
			while (matcher.find()) {
				String link = matcher.group(1);
				if (!link.contains(":")
						&& compararLinkContraExpresionesRegulares(link)) {
					if (link.contains("|")) {
						link = link.substring(0, link.lastIndexOf('|'));
					}
					// Wikipedia tiene links vacios, ejemplo, ver articulo
					// "Comarca de Fonsagrada" en la Wikipedia en Español
					if (!link.equals("")) {
						link = convertirPrimerLetraAMayuscula(link);
						// Un enlace puede tener tabs dentro, los removemos
						links.add(removerTabs(link));
					}
				}
			}
		}
		return links;
	}

	private Set<String> parseCategories(String text) {
		Set<String> categories = new HashSet<String>();
		if (text != null) {
			Matcher matcher = CATEGORY_PATTERN.matcher(text);
			while (matcher.find()) {
				String match = matcher.group();
				categories.add(convertirPrimerLetraAMayuscula(match.substring(
						2, match.length() - 2)));
			}
		}
		return categories;
	}

	/**
	 * Convierte la primer letra del enlace en mayuscula (dado que, por
	 * convención de Wikipedia, todos los artículos comienzan con mayuscula)
	 * 
	 * @param enlace
	 * @return
	 */
	private String convertirPrimerLetraAMayuscula(String enlace) {
		Character letra = enlace.charAt(0);
		if (Character.isLowerCase(letra)) {
			letra = Character.toUpperCase(letra);
			enlace = letra + enlace.substring(1);
		}
		return enlace;
	}

	/**
	 * Permite comparar un link particular contra expresiones regulares
	 * aportadas por el usuario. Si algun link coincide con dichas expresiones,
	 * se devuelve Boolean.FALSE.
	 * 
	 * @param link
	 * @return
	 */
	private Boolean compararLinkContraExpresionesRegulares(String link) {
		if (this.expresionesRegulares == null) {
			return Boolean.TRUE;
		} else {
			Iterator<String> iterador = this.expresionesRegulares.iterator();
			String expReg;
			while (iterador.hasNext()) {
				expReg = (String) iterador.next();
				if (Pattern.matches(expReg.toString(), link)) {
					return Boolean.FALSE;
				}
			}
			return Boolean.TRUE;
		}
	}

	private String removerTabs(String enlace) {
		return enlace.replace("\t", "");
	}
}
