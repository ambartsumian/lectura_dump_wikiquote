package tesina;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;

/**
 * Esta clase deberá ser posible de ser invocada mediante una consola, es decir,
 * tiene capacidad para ejecutarse como un programa java por separado.<br>
 * Nos transforma un archivo XML proveniente de una wiki en un archivo de
 * formato txt , apto para ser cargado en Giraph. Tiene un parametro opcional,
 * en donde se listaran las expresiones regulares con las que el usuario puede
 * decidir no incluir en el archivo de datos apto para Giraph.
 * 
 * <br>
 * En este archivo se listara cada vertice, con sus respectivos enlaces y
 * categorias. Estarán como vertices tanto los articulos como las categorias.<br>
 * 
 * <ul>
 * <li>vertice</li>
 * <li>pesoVertice</li>
 * <ul>
 * <li>enlace1</li>
 * <li>
 * pesoEnlace1</li>
 * <li>categoria1</li>
 * <li>pesoCategoria1</li>
 * </ul>
 * </ul> <br>
 * <ul>
 * <li>categoria1</li>
 * <li>pesoVertice</li>
 * <ul>
 * <li>enlaceArticulo1Categoria1</li>
 * <li>
 * pesoEnlaceArticuloCategoria1</li>
 * <li>enlaceArticulo2Categoria1</li>
 * <li>pesoEnlaceArticulo2Categoria1</li>
 * </ul>
 * </ul>
 * 
 * 
 * @author jlarroque
 *
 */
public class ExtractGraph {
	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			System.out
					.println("Uso: ExtractGraph propio <input-file> <output-file> <archivo-con-expresiones-regulares");
			System.exit(255);
		}
		ExtractGraph self = new ExtractGraph();
		self.extract(args[0], args[1], (args.length == 3) ? args[2] : null);
	}

	private void extract(String inputFile, String outputFile,
			String nombreArchivoDeExpresionesRegulares) throws IOException,
			XMLStreamException {
		System.out.println("Parseando frases y extrayendo el grafo...");

		long startTime = System.currentTimeMillis();

		File newTextFile = new File(outputFile);
		File verticesTextFile = new File("vertices.txt");

		List<String> expresionesRegulares = null;
		if (nombreArchivoDeExpresionesRegulares != null) {
			expresionesRegulares = leerArchivoDesdeDisco(nombreArchivoDeExpresionesRegulares);
		}

		FileWriter writer = new FileWriter(newTextFile);
		FileWriter writerVertices = new FileWriter(verticesTextFile);

		GraphExtractor graphExtractor = new GraphExtractor(writer,
				writerVertices, expresionesRegulares);

		graphExtractor.parse(inputFile);

		writer.close();
		writerVertices.close();

		long elapsedSeconds = (System.currentTimeMillis() - startTime) / 1000;
		System.out.printf("\n%d articulos procesados en %d segundos.\n",
				graphExtractor.getPageCount(), elapsedSeconds);
	}

	public static List<String> leerArchivoDesdeDisco(String direccionDelArchivo) {
		List<String> archivoLeido = new ArrayList<String>();

		System.out.println("El archivo " + direccionDelArchivo
				+ " existe, se lo abre para su lectura");
		try {
			archivoLeido = Files.readAllLines(Paths.get(direccionDelArchivo),
					java.nio.charset.Charset.defaultCharset());
		} catch (IOException e) {

			System.out.println("El archivo " + direccionDelArchivo
					+ " no existe en el FS " + e.toString());
		}

		return archivoLeido;
	}
}
