package constantes;

import java.util.regex.Pattern;

/**
 * @author jlarroque
 *
 */
public class ConstantesGeneracionDeGrafoGiraph {

	public static final Pattern LINK_PATTERN = Pattern
			.compile("\\[\\[(.+?)\\]\\]");

	public static final Pattern CATEGORY_PATTERN = Pattern
			.compile("\\[\\[Categoría:(.+?)\\]\\]");
	

}
